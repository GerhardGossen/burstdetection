package de.l3s.gossen.burstdetection;

import java.util.Collection;
import java.util.List;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

import de.l3s.gossen.burstdetection.BurstFinder.CostFunction;

import static de.l3s.gossen.burstdetection.KleinbergBurstDetector.DEFAULT_DENSITY_SCALING;
import static de.l3s.gossen.burstdetection.KleinbergBurstDetector.DEFAULT_GAMMA;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class BurstDetectorTest {
    private final int[] documentFrequencies = new int[] { 100, 100, 100, 100, 100, 100, 100 };
    private final double[] objectFreqencies = new double[] { 1, 1, 1, 1, 100, 100, 1 };
    private final Logger logger = LoggerFactory.getLogger(BurstDetectorTest.class);

    @Test
    public void testBurstDetector1() {
        KleinbergBurstDetector burstDetector = new KleinbergBurstDetector(new int[] { 1000000,
                1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000,
                1000000, 1000000, 1000000, 1000000, 1000000, 1000000 }, 3, 0, DEFAULT_GAMMA,
                DEFAULT_DENSITY_SCALING);
        Collection<Burst> bursts = burstDetector.detectBursts(new double[] { 8718, 7041, 6544,
                5726, 6106, 4251, 13589, 9342, 6055, 5281, 4617, 4428, 3904, 3287, 2749, 20000 });

        for (Burst b : bursts) {
            System.out.println(b);
        }

        List<Burst> burstList = Lists.newArrayList(bursts);
        assertThat(bursts, hasSize(6));
        assertThat(burstList.get(3).getStart(), equalTo(15));
        assertThat(burstList.get(3).getEnd(), equalTo(16));
    }

    @Test
    public void testBurstDetector() {
        final double normalValue = DensityScaledCost.computeExpected(objectFreqencies, documentFrequencies);
        KleinbergBurstDetector burstDetector = new KleinbergBurstDetector(documentFrequencies, 1,
                0, DEFAULT_GAMMA, DEFAULT_DENSITY_SCALING);
        Collection<Burst> bursts = burstDetector.detectBursts(objectFreqencies);
        System.out.println(bursts);
        assertEquals(1, bursts.size());
        Burst burst = bursts.iterator().next();
        assertEquals(4, burst.getStart());
        assertEquals(6, burst.getEnd());

        final int positiveStates = 2;
        final int negativeStates = 0;
        DensityScaledCost costFunction = new DensityScaledCost(positiveStates, negativeStates,
                DEFAULT_DENSITY_SCALING, normalValue, documentFrequencies);
        double transCost = BurstFinder.transCost(documentFrequencies.length, DEFAULT_GAMMA);
        BurstFinder burstFinder = new BurstFinder(positiveStates, negativeStates, costFunction, transCost);
        Collection<Burst> detected = burstFinder.detectBursts(objectFreqencies);
        System.out.println(detected);
        assertThat(detected, hasSize(1));
        assertThat("Old = new", detected.iterator().next(), equalRange(burst));
    }

    private Matcher<Burst> equalRange(final Burst burst) {
        return new BaseMatcher<Burst>() {

            @Override
            public boolean matches(Object item) {
                if (item instanceof Burst) {
                    Burst o = (Burst) item;
                    return burst.getStart() == o.getStart() && burst.getEnd() == o.getEnd();
                } else {
                    return false;
                }
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("expected").appendValue(burst);
            }};
    }

    @Test
    public void testNegativeStates() throws Exception {
        final int positiveStates = 2;
        final int negativeStates = 1;
        final double normalValue = 1.0;
        final double densityScaling = 1.0;
        final double gamma = 0.1;

        CostFunction costFunction = new DeviationBasedCost(positiveStates, negativeStates, densityScaling,
                objectFreqencies, normalValue);
        double transCost = BurstFinder.transCost(documentFrequencies.length, gamma);
        logger.debug("transCost={}", transCost);
        BurstFinder burstFinder = new BurstFinder(positiveStates, negativeStates, costFunction,
                transCost);
        Collection<Burst> detected = burstFinder.detectBursts(objectFreqencies);
        System.out.println(detected);
        assertThat(detected, hasSize(1));
    }

    @Test
    public void testMultipleStates() throws Exception {
        final int positiveStates = 4;
        final int negativeStates = 3;
        final double normalValue = 1.0;
        final double densityScaling = 1.0;
        final double gamma = 0.1;

        CostFunction costFunction = new DeviationBasedCost(positiveStates, negativeStates,
                densityScaling, objectFreqencies, normalValue);
        double transCost = BurstFinder.transCost(documentFrequencies.length, gamma);
        logger.debug("transCost={}", transCost);
        BurstFinder burstFinder = new BurstFinder(positiveStates, negativeStates, costFunction,
                transCost);
        Collection<Burst> detected = burstFinder.detectBursts(objectFreqencies);
        System.out.println(detected);
        assertThat(detected, hasSize(2));
        List<Burst> detectedList = Lists.newArrayList(detected);
        assertThat(detectedList.get(0), equalRange(detectedList.get(1)));
    }

}
