package de.l3s.gossen.burstdetection;

import com.google.common.base.Preconditions;

/** Evaluation result matrix */
public class ConfusionMatrix {
    private int truePositives;
    private int falsePositives;
    private int falseNegatives;
    private int trueNegatives;

    /** Initialize the confusion matrix with the given values. */
    public ConfusionMatrix(int truePositives, int falsePositives,
            int falseNegatives, int trueNegatives) {
        this.truePositives = truePositives;
        this.falsePositives = falsePositives;
        this.falseNegatives = falseNegatives;
        this.trueNegatives = trueNegatives;
    }

    /** Initialize the matrix with 0 for all values. */
    public ConfusionMatrix() {
        this(0, 0, 0, 0);
    }

    /** Get the number of true positives. */
    public int getTruePositive() {
        return truePositives;
    }

    /** Increment the number of true positives. */
    public void addTruePositives(int n) {
        truePositives += checkPositive(n);
    }

    /** Get the number of true negatives. */
    public int getTrueNegative() {
        return trueNegatives;
    }

    /** Increment the number of true negatives. */
    public void addTrueNegative(int n) {
        trueNegatives += n;
    }

    /** Get the number of false positives. */
    public int getFalsePositive() {
        return falsePositives;
    }

    /** Increment the number of false positives. */
    public void addFalsePositives(int n) {
        falsePositives += n;
    }

    /** Get the number of false negatives. */
    public int getFalseNegative() {
        return falseNegatives;
    }

    /** Increment the number of false negatives. */
    public void addFalseNegative(int n) {
        falseNegatives += n;
    }

    /** Add the values of another matrix to this matrix. */
    public void add(ConfusionMatrix other) {
        Preconditions.checkNotNull(other);
        truePositives += other.truePositives;
        falsePositives += other.falsePositives;
        falseNegatives += other.falseNegatives;
        trueNegatives += other.trueNegatives;
    }

    /** Calculate the precision. */
    public double getPrecision() {
        return getTruePositive()
                / ((double) getTruePositive() + getFalsePositive());
    }

    /** Calculate the recall. */
    public double getRecall() {
        return getTruePositive()
                / ((double) getTruePositive() + getFalseNegative());
    }

    /** Calculate the F1 measure. */
    public double getF1() {
        double precision = getPrecision();
        double recall = getRecall();
        return 2 * ((precision * recall) / (precision + recall));
    }

    /**
     * Ensure that the parameter is positive
     * 
     * @param n
     *            an integer, should be positive
     * @return n, if it is positive
     * @throws IllegalArgumentException
     *             if n is negative ( < 0)
     */
    private int checkPositive(int n) {
        Preconditions.checkArgument(n >= 0, "Expected positive value");
        return n;
    }
}
