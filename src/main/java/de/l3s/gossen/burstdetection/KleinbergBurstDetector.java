package de.l3s.gossen.burstdetection;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.l3s.gossen.burstdetection.BurstFinder.CostFunction;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Implementation of Kleinberg's burst detection algorithm.
 */
public class KleinbergBurstDetector implements BurstDetector {

    public static final double DEFAULT_DENSITY_SCALING = 1.5;
    public static final double DEFAULT_GAMMA = 1.0;
    public static final int DEFAULT_STATES = 3;
    private static Logger logger = LoggerFactory.getLogger(KleinbergBurstDetector.class);

    private final int[] documentFrequencies;
    private final double gamma;
    private final double densityScaling;
    private final int positiveStates;
    private final int negativeStates;

    /**
     * Initialize the burst detector.
     * 
     * @param documentFrequencies
     *            number of total documents for each time interval
     * @param positiveStates
     *            number of positive states
     * @param negativeStates
     *            number of negative states
     * @param gamma
     *            cost of transitions
     * @param densityScaling
     *            density of states
     */
    public KleinbergBurstDetector(int[] documentFrequencies, int positiveStates,
            int negativeStates, double gamma, double densityScaling) {
        checkArgument(densityScaling > 1, "Density scaling factor must be > 1");
        this.positiveStates = positiveStates + 1;
        this.negativeStates = negativeStates;
        this.documentFrequencies = documentFrequencies;
        this.gamma = gamma;
        this.densityScaling = densityScaling;
    }

    /** Initialize with default parameter values. */
    public KleinbergBurstDetector(int[] documentFrequencies) {
        this(documentFrequencies, DEFAULT_STATES, 0, DEFAULT_GAMMA, DEFAULT_DENSITY_SCALING);
    }

    @Override
    public Collection<Burst> detectBursts(double[] values) {
        checkArgument(values.length == documentFrequencies.length,
                "#values (%s) != #documentFrequencies (%s)", values.length,
                documentFrequencies.length);

        double expected = DensityScaledCost.computeExpected(values, documentFrequencies);
        logger.debug("expected={}", expected);
        double transCost = BurstFinder.transCost(values.length, gamma);
        logger.debug("transCost={}", transCost);
        CostFunction costFunction = new DensityScaledCost(positiveStates, negativeStates,
                densityScaling, expected, documentFrequencies);
        BurstFinder burstFinder = new BurstFinder(positiveStates, negativeStates,
                costFunction, transCost);

        return burstFinder.detectBursts(values);
    }

}
