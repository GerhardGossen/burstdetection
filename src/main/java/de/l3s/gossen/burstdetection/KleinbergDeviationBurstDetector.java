package de.l3s.gossen.burstdetection;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.l3s.gossen.burstdetection.BurstFinder.CostFunction;

import static com.google.common.base.Preconditions.checkArgument;
import static de.l3s.gossen.burstdetection.KleinbergBurstDetector.DEFAULT_GAMMA;
import static de.l3s.gossen.burstdetection.KleinbergBurstDetector.DEFAULT_STATES;

/**
 * Adaption of Kleinberg's burst detection algorithm that distributes states
 * based on the standard deviation of the input data.
 */
public class KleinbergDeviationBurstDetector implements BurstDetector {

    public static final double DEFAULT_DEV_DENSITY_SCALING = 0.25;
    private static final Logger logger = LoggerFactory.getLogger(KleinbergDeviationBurstDetector.class);
    private final int inputStates;
    private final double gamma;
    private final double densityScaling;

    /**
     * Initialize the burst detector
     * 
     * @param inputStates
     *            number of burst states for each direction
     * @param gamma
     *            cost of transitions
     * @param densityScaling
     *            multiple of standard deviations used as difference between
     *            states
     */
    public KleinbergDeviationBurstDetector(int inputStates, double gamma, double densityScaling) {
        this.inputStates = inputStates;
        this.gamma = gamma;
        this.densityScaling = densityScaling;
    }

    /** Initialize with default parameter values. */
    public KleinbergDeviationBurstDetector() {
        this(DEFAULT_STATES, DEFAULT_GAMMA, DEFAULT_DEV_DENSITY_SCALING);
    }

    @Override
    public Collection<Burst> detectBursts(double[] values) {
        checkArgument(values.length > 0, "no data provided");

        CostFunction costFunction = new DeviationBasedCost(inputStates + 1, inputStates,
                densityScaling, values);
        double transCost = BurstFinder.transCost(values.length, gamma);
        logger.debug("transCost={}", transCost);

        BurstFinder burstFinder = new BurstFinder(inputStates + 1, inputStates, costFunction,
                transCost);

        return burstFinder.detectBursts(values);
    }


}
