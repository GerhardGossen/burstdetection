package de.l3s.gossen.burstdetection;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

import de.l3s.gossen.burstdetection.Burst.Direction;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static de.l3s.gossen.burstdetection.Burst.Direction.HIGHER;
import static de.l3s.gossen.burstdetection.Burst.Direction.LOWER;
import static java.lang.Math.abs;

/**
 * Generic burst detection method based on Kleinberg's algorithm.
 * 
 * This method can find bursts in both directions (above and below average) and
 * assigns costs to states based on a user-specified function.
 * 
 */
public class BurstFinder {
    /** Function to calculate the cost of a state. */
    public static interface CostFunction {
        /**
         * Find the cost for a specific state for a given value
         * 
         * @param level
         *            the level of the state (can be negative)
         * @param value
         *            the value of the input array
         * @param cell
         *            the index of the value in the input array
         * @return the calculated cost value
         */
        double calculate(int level, double value, int cell);
    }

    private static final double DTRANS = 1.0;
    private static final Logger logger = LoggerFactory.getLogger(BurstFinder.class);
    private final double transitionCost;
    private final int negativeStates;
    private final int positiveStates;
    private final CostFunction costFunction;
    private final int totalStates;

    /**
     * Construct a new burst finder.
     * 
     * @param positiveStates
     *            number of non-negative states (must be >= 2)
     * @param negativeStates
     *            number of negative states (can be 0)
     * @param costFunction
     *            function to calculate the costs for each state
     * @param transitionCost
     *            cost for transitions between states
     * @see #transCost(int, double)
     */
    public BurstFinder(int positiveStates, int negativeStates,
            CostFunction costFunction, double transitionCost) {
        checkArgument(positiveStates >= 2, "must specify at least ground state + 1 burst state");
        checkArgument(negativeStates >= 0, "negativeStates must be >= 0");
        checkArgument(transitionCost >= 0, "transitionCost must be >= 0");
        logger.debug(
                "Creating burstfinder with parameters positive={}, negative={}, transitionCost={}",
                positiveStates, negativeStates, transitionCost);
        this.positiveStates = positiveStates;
        this.negativeStates = negativeStates;
        this.costFunction = checkNotNull(costFunction);
        this.transitionCost = transitionCost;
        this.totalStates = positiveStates + negativeStates;
    }

    /** Find the bursts in the input data. */
    public Collection<Burst> detectBursts(double[] values) {
        double[][] costs = computeCosts(values, costFunction);
        logger.debug("costs={}", (Object) costs);

        double[][] totalCosts = new double[values.length][totalStates];
        int[][] paths = new int[values.length][totalStates];

        computePaths(costs, totalCosts, paths);
        logger.debug("paths={}", (Object) paths);
        int[] optimalSequence = findOptimalSequence(totalCosts, paths);
        logger.debug("Optimal sequence = {}", optimalSequence);

        Collection<Burst> bursts = extractBursts(totalCosts, optimalSequence);
        return bursts;
    }

    /**
     * Retrieve the bursts for a given state sequence.
     * 
     * @param totalCosts
     *            the matrix of total costs for a cell (intrinsic + minimal path
     *            up to this node)
     * @param optimalSequence
     *            an optimal sequence of states
     * @return the bursts for this sequences, or an empty collection. The bursts
     *         are ordered according to their start index.
     */
    private Collection<Burst> extractBursts(double[][] totalCosts, int[] optimalSequence) {
        Collection<Burst> bursts = Lists.newArrayList();
        // burst level -> start cell of current burst
        int[] burstStarts = new int[totalStates];
        double[] strengths = new double[totalStates];
        for (int i = 0; i < burstStarts.length; i++) {
            burstStarts[i] = -1;
        }

        int lastState = 0;
        for (int cell = 0; cell < optimalSequence.length; cell++) {
            int state = optimalSequence[cell];
            if (state == lastState) { // in burst / base state
                updateStrengths(strengths, cell, state, totalCosts);
                continue;
            } else if (state > 0 && state > lastState) { // start positive burst
                updateStrengths(strengths, cell, state, totalCosts);
                for (int i = lastState + 1; i <= state; i++) {
                    burstStarts[level2Index(i)] = cell;
                    logger.trace("start of burst level={}, cell={}", i, cell);
                }
            } else if (state < 0 && state < lastState) { // start negative burst
                updateStrengths(strengths, cell, state, totalCosts);
                for (int i = lastState - 1; i >= state; i--) {
                    burstStarts[level2Index(i)] = cell;
                    logger.trace("start of burst level={}, cell={}", state, i);
                }
            } else { // end of burst
                if (state >= 0) {
                    for (int i = lastState; i > state; i--) {
                        if (i != 0) {
                            bursts.add(produceBurst(burstStarts, strengths, cell, i, HIGHER));
                        }
                    }
                } else {
                    for (int i = lastState; i < state; i++) {
                        if (i != 0) {
                            bursts.add(produceBurst(burstStarts, strengths, cell, i, LOWER));
                        }
                    }
                }
            }
            lastState = state;
        }

        // finish bursts that are ongoing at end of data
        if (lastState > 0) {
            for (int i = lastState; i > 0; i--) {
                bursts.add(produceBurst(burstStarts, strengths, optimalSequence.length, i, HIGHER));
            }
        } else if (lastState < 0) {
            for (int i = lastState; i < 0; i++) {
                bursts.add(produceBurst(burstStarts, strengths, optimalSequence.length, i, LOWER));
            }
        }
        return bursts;
    }

    /** Update the strength of all current bursts. */
    private void updateStrengths(double[] strengths, int cell, int level, double[][] totalCosts) {
        if (level >= 0) {
            for (int i = 1; i <= level; i++) {
                updateStrength(strengths, cell, i, totalCosts);
            }
        } else {
            for (int i = -1; i >= level; i--) {
                updateStrength(strengths, cell, i, totalCosts);
            }
        }
    }

    /** Update the strength of the current burst at <code>level</code>. */
    private void updateStrength(double[] strengths, int cell, int level, double[][] totalCosts) {
        int idx = level2Index(level);
        strengths[idx] += totalCosts[cell][level2Index(0)] - totalCosts[cell][idx];
    }

    /** Extract the burst at <code>level</code> and reset the corresponding values. */
    private Burst produceBurst(int[] burstStarts, double[] strengths, int endCell, int level,
            Direction direction) {
        int index = level2Index(level);
        int start = burstStarts[index];
        double strength = strengths[index];
        logger.trace("end   of burst level={}, cell={}", level, endCell);
        burstStarts[index] = -1;
        strengths[index] = 0.0;
        if (start < 0 || endCell < 0 || start > endCell) {
            throw new IllegalStateException("Illegal state boundaries: start=" + start + ", end=" + endCell);
        }
        return new Burst(start, endCell, level, strength, direction);
    }

    /**
     * Extract the optimal state sequence based on the previous-links in
     * <code>paths</code>.
     */
    private int[] findOptimalSequence(double[][] totalCosts, int[][] paths) {
        int[] sequence = new int[totalCosts.length];
        double bestCost = totalCosts[totalCosts.length - 1][level2Index(-negativeStates)];
        int bestState = -negativeStates;
        for (int state = -negativeStates + 1; state < positiveStates; state++) {
            double cost = totalCosts[totalCosts.length - 1][level2Index(state)];
            if ((abs(state) < abs(bestState) && cost <= bestCost) || cost < bestCost) {
                bestCost = cost;
                bestState = state;
            }
        }
        sequence[sequence.length - 1] = bestState;
        for (int i = sequence.length - 2; i >= 0; i--) {
            sequence[i] = paths[i][level2Index(sequence[i + 1])];
        }
        return sequence;
    }

    /**
     * Calculate for each (cell, level) pair the best previous path using
     * dynamic programming.
     * 
     * @param costs
     *            the intrinsic costs
     * @param totalCosts
     *            empty array of the same dimensions as costs, will contain the
     *            minimal cost of a path to each cell
     * @param paths
     *            empty array of the same dimensions as costs, will contain the
     *            previous level of a best path to each cell
     */
    private void computePaths(double[][] costs, double[][] totalCosts, int[][] paths) {
        // first bucket can only transition from state 0
        for (int level = -negativeStates; level < positiveStates; level++) {
            totalCosts[0][level2Index(level)] = costs[0][level2Index(level)]
                    + transitionCost(0, level);
        }
        for (int cell = 1; cell < totalCosts.length; cell++) {
            for (int level = -negativeStates; level < positiveStates; level++) {
                int q = -negativeStates;
                double best = totalCost(costs, totalCosts, cell, -negativeStates, level);
                for (int prevLevel = -negativeStates +1; prevLevel < positiveStates; prevLevel++) {
                    double cost = totalCost(costs, totalCosts, cell, prevLevel, level);
                    if (((abs(prevLevel) < abs(q)) && cost <= best) || cost < best) {
                        best = cost;
                        q = prevLevel;
                    }
                }
                totalCosts[cell][level2Index(level)] = best;
                paths[cell - 1][level2Index(level)] = q;
            }
        }
    }

    /** Generate the array of costs for each (cell, level) pair. */
    private double[][] computeCosts(double[] values, CostFunction costFunction) {
        double[][] costs = new double[values.length][totalStates];
        for (int cell = 0; cell < values.length; cell++) {
            for (int level = -negativeStates; level < positiveStates; level++) {
                double cost = costFunction.calculate(level, values[cell], cell);
                costs[cell][level2Index(level)] = cost;
            }
        }
        return costs;
    }

    /**
     * Calculate the total cost for a transition from (cellIdx - 1)@fromLevel to
     * cellIdx@toLevel.
     */
    private double totalCost(double[][] costs, double[][] totalCosts, int cellIdx, int fromLevel,
            int toLevel) {
        return totalCosts[cellIdx - 1][level2Index(fromLevel)]
                + costs[cellIdx][level2Index(toLevel)] + transitionCost(fromLevel, toLevel);
    }

    /** Convert level into array index. */
    private int level2Index(int level) {
        return level + negativeStates;
    }

    /**
     * Cost of a transition from level <code>from</code> to level
     * <code>to</code>.
     */
    private double transitionCost(int from, int to) {
        return transitionCost * (abs(from - to));
    }

    /**
     * Calculate the transitions cost.
     * 
     * @param n
     *            number of items
     * @param gamma
     *            cost factor
     */
    public static double transCost(int n, double gamma) {
        double transCost = gamma * Math.log(n + 1) - Math.log(DTRANS);

        if (transCost < 0.0) {
            transCost = 0.0;
        }
        return transCost;
    }

}
