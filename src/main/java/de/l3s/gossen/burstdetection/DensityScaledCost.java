package de.l3s.gossen.burstdetection;

import de.l3s.gossen.burstdetection.BurstFinder.CostFunction;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Cost function based on a specified density between states.
 * 
 * This function corresponds to the cost function of the original Kleinberg's
 * algorithm.
 */
public class DensityScaledCost implements CostFunction {
    private final int negativeStates;
    private final int[] documentFrequencies;
    private final double[] fRate;

    /**
     * 
     * @param positiveStates
     * @param negativeStates
     * @param densityScaling
     * @param expected
     * @param documentFrequencies
     */
    DensityScaledCost(int positiveStates, int negativeStates, double densityScaling,
            double expected, int[] documentFrequencies) {
        checkArgument(positiveStates >= 2,
                "at least 1 ground + 1 burst state are required, got ", positiveStates);
        checkArgument(negativeStates >= 0, "negativeStates must be non-negative");
        this.negativeStates = negativeStates;
        this.documentFrequencies = checkNotNull(documentFrequencies);
        fRate = initializeFRate(expected, positiveStates, negativeStates,
                densityScaling);
    }

    /** Compute the fire rate corresponding to each burst state. */
    private static double[] initializeFRate(double expected, int positiveStates,
            int negativeStates, double densityScaling) {
        checkArgument(densityScaling > 1.0, "densityScaling must be > 1");
        double[] fRate = new double[positiveStates + negativeStates];

        fRate[negativeStates] = expected;

        for (int i = negativeStates + 1; i < negativeStates + positiveStates; i++) {
            fRate[i] = fRate[i - 1] / densityScaling;
        }
        for (int i = negativeStates - 1; i >= 0; i--) {
            fRate[i] = fRate[i + 1] * densityScaling;
        }
        return fRate;
    }

    /** Compute the logarithm of choose(n,k) */
    private static double logChoose(int n, int k) {
        int index;
        double value = 0.0;

        for (index = n; index > n - k; --index) {
            value += Math.log(index);
        }

        for (index = 1; index <= k; ++index) {
            value -= Math.log(index);
        }
        return value;
    }

    private static double binomW(double probability, int k, int n) {
        if (probability >= 1.0) {
            throw new IllegalArgumentException("probability >= 1.0, got " + probability);
        }
        return -1
                * (logChoose(n, k) + k * Math.log(probability) + (n - k)
                        * Math.log(1.0 - probability));
    }

    @Override
    public double calculate(int level, double value, int cell) {
        return binomW(1 / fRate[level + negativeStates], (int) value,
                documentFrequencies[cell]);
    }

    /** Compute the expected value as the average per bin. */
    public static double computeExpected(double[] entry, int[] binBase) {
        double binN = 0;
        int binK = 0;

        for (int i = 0; i < entry.length; i++) {
            binK += entry[i];
            binN += binBase[i];
        }

        if (binN == 0 || binK == 0) {
            throw new RuntimeException("A word bursted on is never used");
        }

        return binN / binK;
    }
}