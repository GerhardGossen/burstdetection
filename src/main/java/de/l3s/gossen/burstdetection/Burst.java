package de.l3s.gossen.burstdetection;

import java.io.PrintWriter;
import java.util.Locale;
import java.util.Objects;

import com.google.common.collect.Range;

/** A detected burst. */
public class Burst {
    /** Direction of the burst. */
    public enum Direction {
        /** Burst is above expected value. */
        HIGHER,
        /** Burst is below expected value. */
        LOWER
    }

    private final int start;
    private final int end;
    private final double strength;
    private final int level;
    private final Direction direction;

    public Burst(int start, int end, int level, double strength, Direction direction) {
        this.start = start;
        this.end = end;
        this.level = level;
        this.strength = strength;
        this.direction = direction;
    }

    /** Get the starting index of the burst (inclusive). */
    public int getStart() {
        return start;
    }

    /** Get the ending index of the burst (exclusive). */
    public int getEnd() {
        return end;
    }

    public Range<Integer> getDuration() {
        return Range.closedOpen(start, end);
    }

    /**
     * Get the level of the burst.
     * 
     * Can be negative, a value of <code>0</code> corresponds to the base state.
     */
    public int getLevel() {
        return level;
    }

    public double getStrength() {
        return strength;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "Burst [%s...%s %d@%f]",
                start, end, level, strength);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end, level, direction, strength);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Burst o = (Burst) obj;
        return start == o.start && end == o.end && level == o.level && direction == o.direction
                && Double.compare(strength, o.strength) == 0;
    }

    /**
     * Print the values of this burst separated by semicolons to the given
     * output.
     */
    public void printCsv(PrintWriter out) {
        out.printf(Locale.ENGLISH, "%s;%s;%d;%3.2f%n", start, end,
                level, strength);
    }

}
