/**
 * Adaptation of Kleinberg's burst detection algorithm.
 * 
 * This algorithm can detect negative bursts (value is significantly below
 * expected value) and automatically distribute states based on the standard
 * deviation of the input data.
 * 
 * @author Gerhard Gossen &lt;gossen@l3s.de>
 */
package de.l3s.gossen.burstdetection;