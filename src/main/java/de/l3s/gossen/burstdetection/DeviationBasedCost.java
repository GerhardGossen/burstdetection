package de.l3s.gossen.burstdetection;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.l3s.gossen.burstdetection.BurstFinder.CostFunction;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Cost function that automatically scales the density of states based on the
 * standard deviation of the input data.
 */
public class DeviationBasedCost implements CostFunction {

    private final static double MIN_STDEV = 0.1;
    private final static Logger logger = LoggerFactory.getLogger(DeviationBasedCost.class);
    /** Half size of interval around a value used to compute the cost. */
    private final double interval;
    /** Distributions corresponding to the inferred states. */
    private final NormalDistribution[] distributions;
    private final int positiveStates;
    private final int negativeStates;
    /**
     * Normalization value, used to scale cost such that a value exactly at the
     * mean of a distributions has a cost of 1.0.
     */
    private final double normalization;


    /**
     * Initialize the cost function.
     * 
     * @param positiveStates
     *            number of positive states
     * @param negativeStates
     *            number of negative states
     * @param densityScaling
     *            density of states as a multiple of standard deviations
     * @param values
     *            input values, used to compute the standard deviation
     */
    public DeviationBasedCost(int positiveStates, int negativeStates, double densityScaling,
            double[] values) {
        this(positiveStates, negativeStates, densityScaling, values, expectedValue(values));
    }

    public DeviationBasedCost(int positiveStates, int negativeStates, double densityScaling,
            double[] values, double expected) {
        checkArgument(positiveStates >= 2, "at least 1 ground + 1 burst state are required, got ",
                positiveStates);
        checkArgument(negativeStates >= 0, "negativeStates must be non-negative");
        checkArgument(densityScaling > 0, "densityScaling must be > 0");
        double standardDeviation = Math.max(standardDeviation(values, expected), MIN_STDEV);
        this.positiveStates = positiveStates;
        this.negativeStates = negativeStates;
        distributions = createLevels(positiveStates, negativeStates, expected,
                standardDeviation, densityScaling);
        interval = densityScaling * standardDeviation;

        NormalDistribution dist0 = distributions[0];
        normalization = dist0.probability(expected - interval, expected + interval);

        if (logger.isDebugEnabled()) {
            double[] means = new double[distributions.length];
            for (int i = 0; i < distributions.length; i++) {
                means[i] = distributions[i].getMean();
            }
            logger.debug("means={}", means);
        }

    }


    @Override
    public double calculate(int level, double value, int cell) {
        double lowerBound = value - interval;
        double upperBound = value + interval;
            NormalDistribution dist = distributions[level + negativeStates];
            double cost;
        if ((level == -negativeStates && value < dist.getMean())
                || (level == +positiveStates && value > dist.getMean())) {
            cost = 1.0;
        } else {
            cost = dist.probability(lowerBound, upperBound) / normalization;
        }
        cost = 1.0 - cost;
        logger.trace("Cost for cell [{},{}]: {} ({})", cell, level, cost, value);
        return cost;
    }

    private static NormalDistribution[] createLevels(int positiveStates, int negativeStates,
            double expected, double standardDeviation, double densityScaling) {
        double delta = standardDeviation * densityScaling;
        NormalDistribution[] distributions = new NormalDistribution[positiveStates + negativeStates];
        for (int i = -negativeStates; i < positiveStates; i++) {
            double shiftedMean = expected + (i * delta);
            distributions[i + negativeStates] = new NormalDistribution(shiftedMean, delta);
        }
        return distributions;
    }

    private static double expectedValue(double[] entry) {
        double binK = 0;

        for (double val : entry) {
            binK += val;
        }

        return binK / entry.length;
    }

    private static double standardDeviation(double[] objectValues, double expected) {
        double accumulator = 0;
        for (double value : objectValues) {
            accumulator += (value - expected) * (value - expected);
        }
        return Math.sqrt(accumulator / objectValues.length);
    }

}
