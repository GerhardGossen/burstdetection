package de.l3s.gossen.burstdetection;

import java.util.Collection;

/** An algorithm to detect bursts in a time series. */
public interface BurstDetector {
    /**
     * Find the bursts in a timeseries.
     * 
     * @param values
     *            the time series, one value per time interval.
     * @return bursts in the input. The indices of the bursts correspond to the
     *         indices of the elements in <code>values</code>. When no bursts
     *         are found, an empty collection is returned.
     */
    public Collection<Burst> detectBursts(double[] values);
}
